import React, { useState, useContext, useEffect, useRef } from "react";
import "./PopUp.scss";
import lend from "./assets/lend2.png";
import monpay from "./assets/monpay.jpg";
import card from "./assets/card.png";
import ebarimt from "./assets/ebarimt.png";
import s300 from "./assets/s300.png";

import { CartContext } from "../../context";
import QRCodeStyling from "qr-code-styling";

// let QRCode = require("qrcode.react");
const qrCode = new QRCodeStyling({
  width: 300,
  height: 300,
  image: monpay,
  dotsOptions: {
    color: "#4267b2",
    type: "rounded",
  },
  imageOptions: {
    crossOrigin: "anonymous",
    margin: 20,
  },
});
const qrCodeLend = new QRCodeStyling({
  width: 300,
  height: 300,
  image: lend,
  dotsOptions: {
    color: "black",
    type: "rounded",
  },
  imageOptions: {
    crossOrigin: "anonymous",
    margin: 20,
  },
});

const PopUp = () => {
  const context = useContext(CartContext);
  const [count, setcount] = useState(1);
  const [pageId, setpage] = useState(0);
  const refMonpay = useRef(null);
  const refLend = useRef(null);
  const [showQR, setshowQR] = useState(false);
  // const [timer, settimer] = useState(45);
  console.table(context.SelectedProduct);
  useEffect(() => {
    switch (pageId) {
      case 2:
        console.log("showqr updating", showQR);
        qrCodeLend.append(refLend.current);
        qrCodeLend.update({
          data: showQR.data.qrcode,
        });
        break;
      case 3:
        qrCode.append(refMonpay.current);
        qrCode.update({
          data: showQR.data.qrcode,
        });
        break;
      default:
        break;
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [showQR]);

  useEffect(() => {
    const fetchQR = async () => {
      switch (pageId) {
        case 1:
          break;
        case 2:
          window.api.send("payment", {
            amount: context.SelectedProduct.price * count,
            type: "lend",
            process: "real",
            product: context.SelectedProduct,
            count: count,
          });
          window.api.receive("payment", (arg) => {
            // eslint-disable-next-line no-console
            console.log("arg");
            console.log(arg.data);
            setshowQR(arg);
          });
          break;
        case 3:
          window.api.send("payment", {
            amount: context.SelectedProduct.price * count,
            type: "monpay",
            process: "real",
          });
          window.api.receive("payment", (arg) => {
            // eslint-disable-next-line no-console
            console.log("arg");
            console.log(arg.data);
            setshowQR(arg);
          });
          break;
        default:
          break;
      }
    };
    fetchQR();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [pageId, count]);
  useEffect(() => {
    if (!context.state) {
      setpage(0);
      setcount(1);
    }
    let myVar;
    clearInterval(myVar);
  }, [context]);

  useEffect(() => {
    setInterval(() => {
      window.api.send("tick", true);
    }, 2000);
    window.api.receive("tick", (arg) => {
      switch (arg) {
        case "success":
          context.setState(!context.state);
          break;
        case "failed":
          context.setState(!context.state);
          break;
        default:
          break;
      }
    });
  }, []);
  const handleAddition = () => {
    if (count < 5) {
      const value = count + 1;
      setcount(value);
    }
  };
  const handleSubtraction = () => {
    if (count > 1) {
      const value = count - 1;
      setcount(value);
    }
  };
  return (
    <div className="popper">
      <div
        className={`popup-backdrop ${context.state}`}
        onClick={() => context.setState(!context.state)}
      ></div>
      <div className={`popup-wrapper ${context.state}`}>
        <div
          className="cancel"
          onClick={() => context.setState(!context.state)}
        >
          X
        </div>

        {context.SelectedProduct !== undefined ? (
          <div>
            {" "}
            <div className="popup-top-flex">
              <div className={`popup-image ${6 > pageId && pageId >= 0}`}>
                <div className="image-wrapper">
                  <img
                    alt="product"
                    src={
                      "https://dashboard.deedee.mn" +
                      context.SelectedProduct.product.images[
                        context.SelectedProduct.product.featuredImageId
                      ]
                    }
                  />
                </div>
                <div className="counter">
                  <div className="button minus" onClick={handleSubtraction}>
                    -
                  </div>
                  <div className="count">{count}</div>
                  <div className="button plus" onClick={handleAddition}>
                    +
                  </div>
                </div>
              </div>
              {/* Тайлбар popup screen */}
              <div className={`popup-description ${pageId > 1}`}>
                <div className="popup-content">
                  <h1>{context.SelectedProduct.product.name}</h1>

                  <section className="performance-facts">
                    <table className="performance-facts__table">
                      <tbody>
                        <tr>
                          <th colSpan={2}>
                            <b>Calories</b>
                            200
                          </th>
                          <td>Calories from Fat 130</td>
                        </tr>
                        <tr className="thick-row">
                          <td colSpan={3} className="small-info">
                            <b>% Daily Value*</b>
                          </td>
                        </tr>
                        <tr>
                          <th colSpan={2}>
                            <b>Total Fat</b>
                            14g
                          </th>
                          <td>
                            <b>22%</b>
                          </td>
                        </tr>
                        <tr>
                          <td className="blank-cell"></td>
                          <th>Saturated Fat 9g</th>
                          <td>
                            <b>22%</b>
                          </td>
                        </tr>
                        <tr>
                          <td className="blank-cell"></td>
                          <th>Trans Fat 0g</th>
                          <td></td>
                        </tr>
                        <tr>
                          <th colSpan={2}>
                            <b>Cholesterol</b>
                            55mg
                          </th>
                          <td>
                            <b>18%</b>
                          </td>
                        </tr>
                        <tr>
                          <th colSpan={2}>
                            <b>Sodium</b>
                            40mg
                          </th>
                          <td>
                            <b>2%</b>
                          </td>
                        </tr>
                        <tr>
                          <th colSpan={2}>
                            <b>Total Carbohydrate</b>
                            17g
                          </th>
                          <td>
                            <b>6%</b>
                          </td>
                        </tr>
                        <tr>
                          <td className="blank-cell"></td>
                          <th>Dietary Fiber 1g</th>
                          <td>
                            <b>4%</b>
                          </td>
                        </tr>
                        <tr>
                          <td className="blank-cell"></td>
                          <th>Sugars 14g</th>
                          <td></td>
                        </tr>
                        <tr className="thick-end">
                          <th colSpan={2}>
                            <b>Protein</b>
                            3g
                          </th>
                          <td></td>
                        </tr>
                      </tbody>
                    </table>
                    <table className="performance-facts__table--grid">
                      <tbody>
                        <tr>
                          <td colSpan={2}>Vitamin A 10%</td>
                          <td>Vitamin C 0%</td>
                        </tr>
                        <tr className="thin-end">
                          <td colSpan={2}>Calcium 10%</td>
                          <td>Iron 6%</td>
                        </tr>
                      </tbody>
                    </table>

                    <table className="performance-facts__table--small small-info">
                      <thead>
                        <tr>
                          <td colSpan={2} />
                          <th>Calories:</th>
                          <th>2,000</th>
                          <th>2,500</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <th colSpan={2}>Total Fat</th>
                          <td>Less than</td>
                          <td>65g</td>
                          <td>80g</td>
                        </tr>
                        <tr>
                          <td className="blank-cell" />
                          <th>Saturated Fat</th>
                          <td>Less than</td>
                          <td>20g</td>
                          <td>25g</td>
                        </tr>
                        <tr>
                          <th colSpan={2}>Cholesterol</th>
                          <td>Less than</td>
                          <td>300mg</td>
                          <td>300 mg</td>
                        </tr>
                        <tr>
                          <th colSpan={2}>Sodium</th>
                          <td>Less than</td>
                          <td>2,400mg</td>
                          <td>2,400mg</td>
                        </tr>
                        <tr>
                          <th colSpan={3}>Total Carbohydrate</th>
                          <td>300g</td>
                          <td>375g</td>
                        </tr>
                        <tr>
                          <td className="blank-cell" />
                          <th colSpan={2}>Dietary Fiber</th>
                          <td>25g</td>
                          <td>30g</td>
                        </tr>
                      </tbody>
                    </table>
                  </section>
                </div>
              </div>
              <div className={`popup-pricing ${6 > pageId && pageId >= 0}`}>
                <section className="pricing">
                  <span>
                    Төлөх дүн :{" "}
                    {" " + context.SelectedProduct.price * count + "₮"}
                  </span>
                </section>
              </div>
              {/* Lend popup screen */}
              <div className={`popup-lend ${pageId !== 2}`}>
                <div className="popup-content">
                  {/* <div className="popup-title">QR кодоо уншуулна уу</div> */}
                  <div ref={refLend} />
                  <div className="timer"></div>
                  <div className="hint">QR кодоо уншуулна уу</div>
                </div>
              </div>
              {/* Monpay popup screen */}
              <div className={`popup-monpay ${pageId !== 3}`}>
                <div className="popup-content">
                  {/* <div className="popup-title">QR кодоо уншуулна уу</div> */}
                  <div ref={refMonpay} />
                  <div className="timer"></div>

                  <div className="hint">QR кодоо уншуулна уу</div>
                </div>
              </div>
              <div className={`popup-card ${pageId !== 4}`}>
                <div className="popup-content">
                  <img alt={"s300"} src={s300}></img>
                  <div className="hint">Картаа уншуулна уу</div>
                </div>
              </div>
              <div className={`popup-ebarimt ${6 > pageId && pageId >= 0}`}>
                <div className="ebarimt-content">
                  <h1>Баярлалаа</h1>
                </div>
              </div>
            </div>
            <div
              className={`popup-button ${pageId !== 0}`}
              onClick={() => {
                setpage(1);
              }}
            >
              Худалдан авах
            </div>
          </div>
        ) : (
          ""
        )}
        <div className={`popup-back ${6 > pageId && pageId >= 1}`}>
          {" "}
          {context.Pos && (
            <div
              className="card"
              onClick={() => {
                setpage(4);
              }}
            >
              <img alt={"card"} src={card}></img>
              Card
            </div>
          )}
          <div
            className="lend"
            onClick={() => {
              setpage(2);
            }}
          >
            <img alt={"lend"} src={lend}></img>
            LendMn
          </div>{" "}
          <div
            className="monpay"
            onClick={() => {
              setpage(3);
            }}
          >
            <img alt={"monpay"} src={monpay}></img>
            Monpay
          </div>
        </div>

        <div className={`popup-back ${pageId === 6} ebarimt`}>
          <div
            className="ebarimt"
            onClick={() => {
              setpage(4);
            }}
          >
            <img alt={"ebarimt"} src={ebarimt}></img>
            Баримт хэвлэх
          </div>
        </div>
      </div>
    </div>
  );
};
export { PopUp };
