import React, { useContext } from "react";
import { Redirect } from "react-router";
import { CartContext } from "../../context";
import Image from "./gear.png";
import "./UnderMaintanence.scss";
const UnderMaintanence = () => {
  const cart = useContext(CartContext);
  if (cart.product.activeStatus) return <Redirect to="/" />;
  else
    return (
      <div className="body">
        <div>
          <header className="App-header">
            <img src={Image} className="App-logo" alt="logo" />
            <p style={{ marginBottom: 0 }}>
              Уучлаарай засвар хийж байгаа тул түр ажиллахгүй
            </p>
            <p style={{ fontSize: 20 }}>Та дараа зочилно уу</p>
          </header>
        </div>
      </div>
    );
};

export { UnderMaintanence };
