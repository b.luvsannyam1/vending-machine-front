import React, { useContext, useState, useEffect } from "react";
import { CartContext } from "../../context";
import Footer from "../Footer";
import PopUp from "../PopUp";
import ProductScroll from "../ProductScroll";
// import ServiceWrapper from "../ServiceWrapper";
import "./ProductWrapper.scss";
const ProductWrapper = () => {
  const context = useContext(CartContext);

  const [productScrolls, setproductScrolls] = useState(false);
  const [counter] = useState(2);
  useEffect(() => {
    const refreshProducts = () => {
      const limiter =
        context.product.inventory.length / counter -
        ((context.product.inventory.length / 2) % 1);
      let data = {};
      let arr = [];
      for (let index = 0; index < counter; index++) {
        data["index" + index] = context.product.inventory.slice(
          0 + index * limiter,
          counter === index
            ? context.product.inventory.length
            : limiter +
                index * limiter +
                (index !== 0
                  ? context.product.inventory.length % 2 === 1
                    ? 1 + (context.product.inventory.length - limiter * counter)
                    : 0
                  : 0)
        );
        arr.push(data["index" + index]);
      }
      setproductScrolls(arr);
    };
    refreshProducts();
  }, [counter, context]);

  return (
    <div className="product-wrapper">
      <PopUp />
      {productScrolls &&
        productScrolls.map((item) => {
          return (
            <ProductScroll key={Math.floor(Math.random() * 500)} items={item} />
          );
        })}
      <Footer />
    </div>
  );
};

export { ProductWrapper };
