import React, {
  // useState,
  useEffect,
} from "react";
import ReactPlayer from "react-player/lazy";
// import Image from "./phone.svg";
import "./VideoPlayer.scss";
const VideoPlayer = () => {
  // const VideoList = [
  //   "https://www.youtube.com/watch?v=ysz5S6PUM-U",
  //   "https://www.youtube.com/watch?v=OqkRo17ZbbY",
  // ];
  // const [Id, setId] = useState(0);
  // const [state, setstate] = useState(null);
  useEffect(() => {
    const setDate = () => {
      // var d = new Date();
      // var n = d.toString();
      // setstate(n);
      setTimeout(setDate, 1000);
    };

    setTimeout(setDate, 1000);
  }, []);

  const handleVideo = () => {};
  return (
    <div className="video-player-wrapper">
      <div className="video-player">
        {" "}
        <ReactPlayer
          width="100%"
          height="100%"
          onEnded={handleVideo}
          url="https://www.youtube.com/watch?v=ysz5S6PUM-U"
        />
      </div>
    </div>
  );
};

export { VideoPlayer };
