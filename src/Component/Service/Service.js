import React from "react";
import "./Service.scss";
const Service = ({ image, text }) => {
  return (
    <div className="service">
      <div className="content">
        <img alt={text} src={image} />
        <div style={{ padding: 10, fontSize: 20 }}>{text}</div>
      </div>
    </div>
  );
};

export { Service };
