import React, { useContext } from "react";
import "./Product.scss";
// import Image from "./example.png";
import { CartContext } from "../../context";

const Product = ({ item }) => {
  const context = useContext(CartContext);
  // console.log(item);
  const handleClick = () => {
    context.setState(!context.state);
    context.setSelectedProduct(item);
  };
  return (
    <div className="product-item" onClick={handleClick}>
      <img
        alt="cola"
        src={
          "https://dashboard.deedee.mn" +
          item.product.images[item.product.featuredImageId]
        }
      ></img>
      <div className="price">{item.price}₮</div>
    </div>
  );
};

export { Product };
