import React, { useState, useEffect } from "react";
import "./Footer.scss";
import Image from "./phone.svg";

const Footer = () => {
  const [state, setstate] = useState(null);
  useEffect(() => {
    const setDate = () => {
      var d = new Date();
      var n = d.toString();
      setstate(n);
      setTimeout(setDate, 1000);
    };

    setTimeout(setDate, 1000);
  }, []);

  return (
    <div className="footer">
      <div className="zar">
        <section>
          <div>
            <section className="news-message">
              <p>
                Mongol uls, Chingeltei duureg , 5-p xopoo Logos tuw 7 davhar
                ,701 toot
              </p>
            </section>
            <section className="news-message">
              <p>
                Mongol uls, Chingeltei duureg , 5-p xopoo Logos tuw 7 davhar
                ,701 toot
              </p>
            </section>
          </div>
        </section>
      </div>
      <div className="time">
        <div className="date">
          <div>info@deedee.mn</div>
          <div className="company">
            Дээдээ машин ХХК <img className="phone" alt="phone" src={Image} />{" "}
            7716-5555
          </div>
          <div>{state && state.substr(0, 21)}</div>
        </div>
        <div className="orange"> </div>
        <div> </div>
      </div>{" "}
      <div className="copyright">
        © 2018-2021 All rights reserved to Deedee Machine LLC
      </div>
    </div>
  );
};
export { Footer };
