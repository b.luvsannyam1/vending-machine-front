import React, { useRef } from "react";
import { useEffect } from "react/cjs/react.development";
import Product from "../Product";
import "./ProductScroll.scss";

const ProductScroll = ({ items }) => {
  // console.table(items);
  const ref = useRef(null);
  useEffect(() => {
    let rate = 1;
    const Scroll = () => {
      setTimeout(() => {
        if (ref !== null && ref.current !== null) {
          ref.current.scrollLeft = ref.current.scrollLeft + rate;
          if (ref.current.scrollLeftMax === ref.current.scrollLeft) {
            rate = -1*0.6;
          }
          if (0 === ref.current.scrollLeft) {
            rate = 1*0.6;
          }
        }
        Scroll();
      }, 60);
    };
    if (ref !== null && ref.current !== null) {
      Scroll();
    }
  }, [ref]);
  return (
    <div className="product-scroll">
      <div className="products" ref={ref}>
        {items.map((item) => {
          return (
            <Product
              key={item.name + Math.floor(Math.random() * 11111)}
              item={item}
            />
          );
        })}
      </div>
      <div className="bottom-deck">
        <div className="deck-top"></div>
        <div className="deck"></div>
        <div className="seam"></div>
        <div className="deck-face"></div>
        <div className="deck-bottom"></div>
      </div>
    </div>
  );
};

export { ProductScroll };
