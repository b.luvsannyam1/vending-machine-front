import React, { useContext } from "react";

import { CartContext } from "../../context";
import { authContext } from "../../context/auth";
import { Redirect } from "react-router-dom";
import "./SelectMachine.scss";
const SelectMachine = () => {
  const auth = useContext(authContext);
  const cart = useContext(CartContext);
  console.log(auth);
  if (cart.vendingMachine === false)
    return (
      <div className="selector">
        {auth.state &&
          auth.state.user.data.machines.map((item) => {
            return (
              <div className="machine">
                {item._id}
                <div>Машины нэр: {item.name}</div>
                <div>Түүнтэй холбогдох anydesk: {item.anydesk}</div>
                <button
                  onClick={() => {
                    cart.setVendingMachine(item._id);
                  }}
                >
                  Сонгох
                </button>
              </div>
            );
          })}
      </div>
    );
  else {
    return <Redirect to="/vending" />;
  }
};
export { SelectMachine };
