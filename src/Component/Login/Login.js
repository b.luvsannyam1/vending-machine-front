import React, { useState, useContext } from "react";
import "./Login.scss";
import Image from "./image.png";
import { authContext } from "../../context/auth";

const Login = () => {
  const [username, setusername] = useState("");
  const [password, setpassword] = useState("");
  const user = useContext(authContext);
  const handleLogin = () => {
    console.log("initing login", window.api);
    window.api.send("auth", { username, password });
    window.api.receive("auth", (arg) => {
      // eslint-disable-next-line no-console
      user.setstate(arg);
    });
  };
  return (
    <div className="login-wrapper">
      <div className="login-container">
        <div className="login logo">
          <img alt="logo" src={Image} />
        </div>
        <h2>Дээдээ машин</h2>
      </div>
      <div className="login-content">
        <input
          value={username}
          placeholder="Нэвтрэх нэр"
          onChange={(e) => {
            setusername(e.target.value);
          }}
        />
        <input
          value={password}
          placeholder="Нууц үг"
          onChange={(e) => {
            setpassword(e.target.value);
          }}
        />
        <button onClick={() => handleLogin()}>Нэвтрэн орох</button>
      </div>
    </div>
  );
};
export { Login };
