import React, { useContext } from "react";
import { Redirect } from "react-router-dom";
import { ProductWrapper } from "../ProductWrapper/ProductWrapper";
import VideoPlayer from "../VideoPlayer";

import { CartContext } from "../../context";
import "./Vending.scss";
const Vending = () => {
  const cart = useContext(CartContext);

  if (cart.vendingMachine !== false)
    if (!cart.product.activeStatus) return <Redirect to="/maintanence" />;
    else
      return (
        <div className="vending-wrapper">
          <VideoPlayer />
          <ProductWrapper />
        </div>
      );
  else return <Redirect to="/" />;
};

export { Vending };
