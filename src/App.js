// import logo from "./logo.svg";
import "./App.css";
import React, { useContext } from "react";
import Vending from "./Component/Vending";
import { CartProvider } from "./context";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import Login from "./Component/Login";
import { ProvideAuth } from "./context/auth";
import { authContext } from "./context/auth";
import SelectMachine from "./Component/SelectMachine";
import UnderMaintanence from "./Component/UnderMaintanence";

function Navigation() {
  const user = useContext(authContext);
  console.log(user);
  if (user.state)
    return (
      <Switch>
        <Route path="/vending">
          <Vending />
        </Route>
        <Route path="/maintanence">
          <UnderMaintanence />
        </Route>
        <Route path="/">
          <SelectMachine />
        </Route>
      </Switch>
    );
  else {
    return (
      <Route path="/">
        <Login />
      </Route>
    );
  }
}

function App() {
  return (
    <Router>
      <div className="App">
        <ProvideAuth>
          <CartProvider>
            <Navigation />
          </CartProvider>
        </ProvideAuth>
      </div>
    </Router>
  );
}

export default App;
