import React, { createContext, useState } from "react";

const authContext = createContext();

function ProvideAuth({ children }) {
  const [state, setstate] = useState();
  return (
    <authContext.Provider value={{ state, setstate }}>
      {children}
    </authContext.Provider>
  );
}

export { ProvideAuth, authContext };
