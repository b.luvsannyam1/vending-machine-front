import React, { createContext, useState, useEffect } from "react";

import { io } from "socket.io-client";
export const CartContext = createContext();

const ENDPOINT = "http://localhost:5000/vending-socket";
export const CartProvider = ({ children }) => {
  const [state, setState] = useState(false);
  const [SelectedProduct, setSelectedProduct] = useState();
  const [product, setproduct] = useState({ inventory: [], activeStatus: true });
  const [vendingMachine, setVendingMachine] = useState(false);
  const [Pos, setPos] = useState(false);
  console.log(product);
  useEffect(() => {
    console.log(vendingMachine);
    const fetchProducts = async () => {
      console.log("initing fetchdate", window.api);
      await window.api.send("fetchProducts", vendingMachine);
      await window.api.receive("fetchProducts", (arg) => {
        setproduct(arg.data[0]);
      });
      await window.api.send("connectPos", vendingMachine);
      await window.api.receive("connectPos", (arg) => {
        setPos(arg);
      });
    };
    // eslint-disable-next-line no-unused-vars
    if (vendingMachine !== false) {
      fetchProducts();
      console.log("vendingMachine logged in");
      window.api.send("getVendingMachine", vendingMachine);
      window.api.receive("getVendingMachine", (arg) => {
        console.log("Pos ");
        console.log(arg);
        setPos(arg);
      });
    }
  }, [vendingMachine]);

  return (
    <CartContext.Provider
      value={{
        state,
        setState,
        product,
        SelectedProduct,
        setSelectedProduct,
        vendingMachine,
        setVendingMachine,
        Pos,
      }}
    >
      {children}
    </CartContext.Provider>
  );
};
