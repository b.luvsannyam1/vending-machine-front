const io = require("socket.io-client");
const electron = require("electron");

const app = electron.app;
const BrowserWindow = electron.BrowserWindow;

const path = require("path");
const isDev = require("electron-is-dev");
const { ipcMain } = require("electron");
const { handleLogin, fetchProducts } = require("./utils/api");
const Deedee = require("./utils/Deedee");
const Pos = require("./utils/Pos");
const Payment = require("./utils/Payment");

let mainWindow;

const arduinoBoard = new Deedee();
const posMachine = new Pos();
const paymentHandler = new Payment(posMachine);
function createWindow() {
  mainWindow = new BrowserWindow({
    width: 1050,
    height: 1680,
    fullscreenable: true,
    autoHideMenuBar: true,
    frame: false,

    // use a preload script
    // frame: false,
    webPreferences: { preload: path.join(__dirname, "utils", "preload.js") },
  });
  mainWindow.setMenuBarVisibility(false);
  mainWindow.loadURL(
    isDev
      ? "http://localhost:3000"
      : `file://${path.join(__dirname, "../build/index.html")}`
  );
  mainWindow.on("blur", () => {
    mainWindow.restore();
    mainWindow.focus();
    mainWindow.setKiosk(true);
  });
  mainWindow.on("closed", () => (mainWindow = null));
}

const socket = io("http://dashboard.deedee.mn/vending-machine");
ipcMain.on("auth", async (event, args) => {
  let response = await handleLogin(args);
  console.log(response);
  await event.reply("auth", response);
});
ipcMain.on("fetchProducts", async (event, args) => {
  console.log(" product received");
  let response = await fetchProducts(args);
  arduinoBoard.connect(response.data[0].serialport);
  await event.reply("fetchProducts", response);
});

ipcMain.on("connectPos", async (event) => {
  let response = posMachine.checkPos();

  await event.reply("connectPos", response);
});
ipcMain.on("connectDeedee", async (event, args) => {
  let response = arduinoBoard.connect(args);
  await event.reply("connectDeedee", response);
});
ipcMain.on("getVendingMachine", async (event, args) => {
  console.log(args);
  socket.on(args, (data) => {
    console.log(data);
    event.reply("connectDeedee", data);
  });
});
ipcMain.on("payment", async (event, args) => {
  console.log(args);
  if (args.type === "card") {
    let a = await posMachine.payment(args.amount);
    if (a === "success") {
      paymentHandler.reset();
      const { product, count } = paymentHandler.getRoute();
      const sendData = (index) => {
        if (count >= index) {
          setTimeout(function () {
            sendData(index++);
          }, 3500);
          arduinoBoard.send("F" + 100 + product.route);
        }
      };
      sendData(0);
    }
  }
  let response = await paymentHandler.initPayment(args);
  await console.log(response);
  await event.reply("payment", {
    success: true,
    data: {
      qrcode: response.qr_string,
      uuid: response.invoiceNumber,
    },
  });
});
ipcMain.on("updateVending", async (event, args) => {});
ipcMain.on("beginCountdown", async (event, args) => {
  for (let index = 0; index < 10; index++) {
    console.log("beginCountdown", index);
    event.reply("beginCountdown", index);
  }
});
ipcMain.on("tick", async (event, args) => {
  let a = await paymentHandler.send();
  console.log(a);
  await event.reply("tick", a);
  if (a === "success") {
    paymentHandler.reset();
    const { product, count } = paymentHandler.getRoute();
    const sendData = (index) => {
      if (count >= index) {
        setTimeout(function () {
          sendData(index++);
        }, 3500);
        arduinoBoard.send("F" + 100 + product.route);
      }
    };
    sendData(0);
  }
});
app.on("ready", createWindow);

app.on("window-all-closed", () => {
  if (process.platform !== "darwin") {
    app.quit();
  }
});

app.on("activate", () => {
  if (mainWindow === null) {
    createWindow();
  }
});
