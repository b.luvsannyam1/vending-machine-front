const axios = require("axios");
// from Login

const handleLogin = async (args) => {
  const { username, password } = args;
  console.log(args);
  var data = JSON.stringify({
    email: username,
    password: password,
  });

  var config = {
    method: "post",
    url: "https://dashboard.deedee.mn/server/v1/admin/auth/login",
    headers: {
      "Content-Type": "application/json",
    },
    data: data,
  };
  let login;
  await axios(config)
    .then(function (response) {
      login = response.data;
    })
    .catch(function (error) {
      console.log(error);
    });

  return await login;
};

// context index
const fetchProducts = async (args) => {
  console.log("vendingMachine", args);
  var config = await {
    method: "get",
    url:
      "https://dashboard.deedee.mn/server/v1/admin/vendingmachine?_id=" + args,
  };
  let products;
  await axios(config)
    .then(function (response) {
      products = response.data;
    })
    .catch(function (error) {
      console.log(error);
    });
  return await products;
};

module.exports = { handleLogin, fetchProducts };
