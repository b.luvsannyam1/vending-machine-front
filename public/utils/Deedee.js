const SerialPort = require("serialport");

module.exports = class Deedee {
  name;
  port;
  status = false;
  Readline = SerialPort.parsers.Readline;
  parser = new this.Readline();

  connect(serialName) {
    try {
      if (serialName) {
        this.port = new SerialPort(serialName);
        this.port.pipe(this.parser);
        this.parser.on("data", console.log);
        this.event.emit("connected");
        this.status = true;
        return this.status;
      } else {
        this.event.emit("failed", "SerialName not found");
        this.connectionFailed();
        this.status = true;
        return false;
      }
    } catch (error) {
      this.status = true;
      return false;
    }
  }
  getStatus() {
    return this.status;
  }
  eventListener(eventString, func) {
    this.event.on(eventString, func);
  }
  subscribe(subscribeString, func) {
    if (this.status) {
      this.port.pipe(this.parser);
      this.parser.on(subscribeString, func);
    } else {
      return "not connected";
    }
  }
  sendData(data) {
    if (this.status) this.port.write(data);
    else {
      return "not connected";
    }
  }
};
