var axios = require("axios");

module.exports = class Pos {
  index = 0;
  checkPos() {
    var config = {
      method: "get",
      url: "http://localhost:27028/ajax/get-status-info",
      headers: {
        Cookie:
          "token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYxNjkxYTBhZjk4NGQ1YmQ1MTljMzVlOCIsImlhdCI6MTYzNDI3Nzg5OCwiZXhwIjoxNjM2ODY5ODk4fQ.aS2nTKRdgLFVXHK7EUVnH3h2LesgbV7XagTsjaxZdPQ",
      },
    };
    try {
      axios(config)
        .then(function (response) {
          try {
            if (response.data.status_code === "ok") {
              this.status = true;
            }
          } catch (e) {
            this.status = false;
          }
        })
        .catch(function (error) {
          // console.log(error);
          this.status = false;
        });
    } catch (e) {
      this.status = false;
    }

    return this.status;
  }
  payment(amount) {
    var data = JSON.stringify({
      service_name: "doSaleTransaction",
      service_params: {
        db_ref_no: this.index,
        amount: amount,
        additional_data: {},
      },
    });

    var config = {
      method: "post",
      url: "http://localhost:27028",
      headers: {
        "Content-Type": "application/json",
        Cookie:
          "token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYxNjkxYTBhZjk4NGQ1YmQ1MTljMzVlOCIsImlhdCI6MTYzNDI3Nzg5OCwiZXhwIjoxNjM2ODY5ODk4fQ.aS2nTKRdgLFVXHK7EUVnH3h2LesgbV7XagTsjaxZdPQ",
      },
      data: data,
    };

    axios(config)
      .then(function (response) {
        console.log(JSON.stringify(response.data));
      })
      .catch(function (error) {
        console.log(error);
      });
  }
};
