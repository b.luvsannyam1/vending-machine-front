var axios = require("axios");
var qs = require("qs");
module.exports = class Payment {
  uuid = "";
  amount = 0;
  type = "none";
  iteration = 0;
  paymentType = "none";
  product = {};
  count = 0;
  constructor(card, port) {
    this.card = card;
    this.serialport = port;
  }
  getRoute() {
    return { product: this.product, count: this.count };
  }
  async initPayment(payment) {
    const { amount, type, process, product, count } = payment;
    this.type = await type;
    this.amount = await amount;
    this.iteration = await 0;
    this.product = product;
    this.count = count;
    let data;
    let config;
    if (payment) {
      console.log("Төлбөрийг " + payment.type + " ээр хийхээр сонгосон байна");
      switch (type) {
        case "lend":
          this.paymentType = "lend";
          console.log("initing lend");
          if (process === "real") {
            data = await qs.stringify({
              amount: payment.amount,
              duration: "45",
              description: "Deedee",
            });
            config = await {
              method: "post",
              url: "https://mgw.lend.mn/api/w/invoices",
              headers: {
                "Content-Type": "application/x-www-form-urlencoded",
                "x-and-auth-token":
                  "MWI0NDEyOTg4Y2Q0ZmNlZmEyNjg4NTA0Y2UxZTc1YzA3ODNlNGNhNDE2MTJkMjdlNGZlNjVjNjU2NDFmODllZQ",
              },
              data: data,
            };
          }

          break;
        case "monpay":
          console.log("initing Monpay");
          if (process === "real") {
            data = await JSON.stringify({
              amount: this.amount,
              displayName: "Deedee",
              generateUuid: true,
            });

            config = await {
              method: "post",
              url: "https://wallet.monpay.mn/rest/branch/qrpurchase/generate",
              headers: {
                Authorization: "Basic QkdfRGVlZGVlIG1hc2hpbjo1ODAyNzEy",
                "Content-Type": "application/json",
              },
              data: data,
            };
          }
          break;
        default:
          break;
      }
    } else {
      console.log("failed");
    }
    let paymentResponse;
    await axios(config)
      .then(async (response) => {
        switch (payment.type) {
          case "lend":
            this.uuid = await response.data.response.invoiceNumber;
            paymentResponse = await {
              qr_string: response.data.response.qr_string,
              invoiceNumber: response.data.response.invoiceNumber,
            };
            break;
          case "monpay":
            console.log(response.data);
            paymentResponse = await response.data.result;
            paymentResponse = await {
              qr_string: response.data.result.qrcode,
              invoiceNumber: response.data.result.uuid,
            };
            break;
          default:
            break;
        }
      })
      .catch(function (error) {
        console.log(error);
      });
    return await paymentResponse;
  }
  reset() {
    this.type = "none";
    this.paymentType = "none";
  }
  async send() {
    await console.log("sending confirmation");
    await console.log(this.type, this.iteration, this.paymentType);
    await console.log(this.product);
    await console.log(this.count);
    let value;
    try {
      let config;
      switch (this.paymentType) {
        case "lend":
          console.log("initinglend", this.uuid);
          config = {
            method: "get",
            url: "https://mgw.lend.mn/api/w/invoices/" + this.uuid,
            headers: {
              "Content-Type": "application/x-www-form-urlencoded",
              "x-and-auth-token":
                "MWI0NDEyOTg4Y2Q0ZmNlZmEyNjg4NTA0Y2UxZTc1YzA3ODNlNGNhNDE2MTJkMjdlNGZlNjVjNjU2NDFmODllZQ",
            },
          };
          break;
        case "monpay":
          console.log("initing monpay");
          config = {
            method: "get",
            url:
              "https://wallet.monpay.mn/rest/branch/qrpurchase/check?uuid=" +
              this.uuid,
            headers: {
              "Content-Type": "application/json",
              Authorization: "Basic QkdfRGVlZGVlIG1hc2hpbjo1ODAyNzEy",
            },
          };
          break;
        case "card":
          console.log("initing card");

          config = {
            method: "get",
            url: "https://mgw.lend.mn/api/w/invoices/" + this.uuid,
            headers: {
              "Content-Type": "application/x-www-form-urlencoded",
              "x-and-auth-token":
                "MWI0NDEyOTg4Y2Q0ZmNlZmEyNjg4NTA0Y2UxZTc1YzA3ODNlNGNhNDE2MTJkMjdlNGZlNjVjNjU2NDFmODllZQ",
            },
          };
          break;
        case "none":
          break;
        default:
          console.log("failed");
          break;
      }
      await axios(config).then(async (response) => {
        try {
          console.log("request");
          console.log(response.data.response.status);
          switch (this.type) {
            case "lend":
              switch (response.data.response.status) {
                case 0:
                  value = "waiting";
                  break;
                case 1:
                  console.log("success");
                  value = "success";
                  break;
                case 3:
                  value = "failed";
                  break;
                case "none":
                  value = "none";
                  break;
                default:
                  break;
              }
              break;
            case "monpay":
              if (response.data.code === 23 || response.data.code === 21) {
                value = "waiting";
              } else {
                value = "success";
              }
              break;
            case "none":
              value = "none";
              break;
            default:
              value = "none";
              break;
          }
        } catch (error) {
          console.error(error);
        }
      });
    } catch (error) {
      console.log(error);
    }
    return value;
  }
  resolve() {}
};
